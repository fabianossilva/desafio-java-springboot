package com.compasso.productms;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.compasso.productms.entity.Produto;
import com.compasso.productms.repository.ProdutoRepository;
import com.compasso.productms.repository.ProdutoSpecificationRepository;
import com.compasso.productms.specification.ProdutoMaxPriceSpecification;
import com.compasso.productms.specification.ProdutoMinPriceSpecification;
import com.compasso.productms.specification.ProdutoNameDescriptionSpecification;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ProdutoRepositoryTest {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private ProdutoSpecificationRepository produtoSpecificationRepository;
	
	
	@Test
    void deleteProductTest(){
		
        Produto produto = new Produto();
        produto.setName("PS5");
        produto.setDescription("Console Sony");
        produto.setPrice(new BigDecimal("5000"));
        
        this.produtoRepository.save(produto);
        
        this.produtoRepository.delete(produto);
        
        Integer countProduto = produtoRepository.findAll().size();
	    Assertions.assertEquals(0, countProduto);
        
    }
	
	@Test
    void createProductTest(){
		
		Produto produto = new Produto();
		//produto.setId(1);
		produto.setName("PS5");
		produto.setDescription("Console Sony");
		produto.setPrice(new BigDecimal("5000"));
		
		produtoRepository.save(produto);
	    
		Integer countProduto = produtoRepository.findAll().size();
	    Assertions.assertEquals(1, countProduto);
    }
	
	@Test
    void updateProductTest(){
		
        Produto produto = new Produto();
        produto.setName("PS5");
        produto.setDescription("Console Sony");
        produto.setPrice(new BigDecimal("5000"));
        
        this.produtoRepository.save(produto);
        
        Produto cloneProduto = produto;
        
        produto.setName("PS4");
        produto.setDescription("Console Sony Antigo");
        produto.setPrice(new BigDecimal("3000"));
        this.produtoRepository.save(produto);
        
       produto = this.produtoRepository.getOne(produto.getId());
       
       Assertions.assertEquals(produto, cloneProduto);
        
    }
	
	@Test
    void searchProductTest(){
		
		Produto produto = new Produto();
        produto.setName("ps5");
        produto.setDescription("console sony");
        produto.setPrice(new BigDecimal("3000"));
        
        this.produtoRepository.save(produto);
        
        produto = new Produto();
        produto.setName("wii");
        produto.setDescription("console nintendo");
        produto.setPrice(new BigDecimal("4000"));
        
        this.produtoRepository.save(produto);
        
		Specification<Produto> specification = Specification
                .where(ProdutoNameDescriptionSpecification.name("ps5"))
                .and(ProdutoMinPriceSpecification.minPrice(new BigDecimal("3000")))
                .and(ProdutoMaxPriceSpecification.maxPrice(new BigDecimal("3000")));
		
		List<Produto> listaFiltrada = produtoSpecificationRepository.findAll(specification);
		
		Assertions.assertEquals(1, listaFiltrada.size());
    }
	
	

}
