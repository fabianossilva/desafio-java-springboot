package com.compasso.productms.controller;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.compasso.productms.entity.Produto;
import com.compasso.productms.service.ProdutoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

@Api(tags = "Operações realizadas no Produto")
@RestController
@CrossOrigin
@RequestMapping(value = "/products")
@RequiredArgsConstructor
public class ProdutoController {
	
	@Autowired
	ProdutoService produtoService;
	
		@ApiOperation(value = "Criação de um produto", notes = "Criação de um produto.")
		@ApiResponses(value = {
				@ApiResponse(code = 201, message = "Produto Criado com Sucesso."),
				@ApiResponse(code = 400, message = "Ocorreu algum erro de validação.")
		})
		@PostMapping
		public ResponseEntity<?> createProduct(HttpServletRequest request,
											   @Valid @RequestBody Produto produto) {
			return this.produtoService.createProduct(request, produto);
		}
		
		
	    @ApiOperation(value = "Atualiza o produto se o mesmo existir")
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "Retorna 200 quando o produto existe e foi atualizado."),
	            @ApiResponse(code = 400, message = "Retorna 400 quando ocorrer algum erro de validação."),
	            @ApiResponse(code = 404, message = "Retorna 404 quando o produto não existe.")
	    })
	    @PutMapping("/{id}")
	    public ResponseEntity<?> updateProduct(HttpServletRequest request,
	    									   @PathVariable("id") Integer id, 
	    									   @Valid @RequestBody Produto produto) {
	        return this.produtoService.updateProduct(request, produto, id);
	    }
	    
	    
	    @ApiOperation(value = "Mostra os dados de um produto por id")
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "Retorna 200 quando o produto existe."),
	            @ApiResponse(code = 404, message = "Retorna 404 quando o produto não existe.")
	    })
	    @GetMapping("/{id}")
	    public ResponseEntity<?> getProductById(HttpServletRequest request,
	    										  @PathVariable("id") Integer id) {
	    	return this.produtoService.getProductById(request, id);
	    }
	    
	    
	    @ApiOperation(value = "Mostra os dados de todos os produtos salvos.")
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "Retorna 200 mesmo que não haja produtos salvos.")
	    })
	    @GetMapping
	    public ResponseEntity<?> getAllProducts(HttpServletRequest request) {
	    	return this.produtoService.getAllProducts(request);
	    }
	    
	    
	    @ApiOperation(value = "Deletar um produto por id")
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "Retorna 200 quando o produto foi deletado."),
	            @ApiResponse(code = 404, message = "Retorna 404 quando o produto não foi encontrado no banco")
	    })
	    @DeleteMapping("/{id}")
	    public ResponseEntity<?> deleteProduct(HttpServletRequest request,
	    									   @PathVariable("id") Integer id) {
	    	return this.produtoService.deleteProduct(request, id);
	    }
	    

	    @ApiOperation(value = "Busca produtos filtrando: q em name ou description, price >= min_price, price <= max_price.")
	    @ApiResponses(value = {
	            @ApiResponse(code = 200, message = "Retorna 200 mesmo que não haja produtos no resultado da busca.")
	    })
	    @GetMapping("/search")
	    public ResponseEntity<?> search(@RequestParam(required = false) String q,
	                                                @RequestParam(required = false) BigDecimal min_price,
	                                                @RequestParam(required = false) BigDecimal max_price) {
	    	return this.produtoService.search(q, min_price, max_price);
	    }
}
