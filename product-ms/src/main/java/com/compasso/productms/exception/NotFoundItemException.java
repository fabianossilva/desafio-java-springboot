package com.compasso.productms.exception;

public class NotFoundItemException extends RuntimeException {

	private static final long serialVersionUID = 3200073089125243430L;

	public NotFoundItemException() {
		super("");
	}

	public NotFoundItemException(String msg) {
		super(msg);
	}

}
