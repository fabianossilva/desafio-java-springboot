package com.compasso.productms.exception.dto;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {
	
	private Integer status_code;
	
    private List<String> message;
    
    public ErrorResponse(Integer status_code, String messageError){
        this.status_code = status_code;
        this.message = Collections.singletonList(messageError);
    }

}
