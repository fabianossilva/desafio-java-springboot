package com.compasso.productms.exception;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ErrorMessage {
	static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm:ss");

	private String dataHora;
	private String mensagem;
	private String recurso;

	public ErrorMessage() {
	}

	public ErrorMessage(String message) {
		this.dataHora = formatter.format(new Date());
		this.mensagem = message;
	}

	public ErrorMessage(Date timestamp, String message, String recurso) {
		this.dataHora = formatter.format(timestamp);
		this.mensagem = message;
		this.recurso = recurso;
	}

	public String getDataHora() {
		return dataHora;
	}

	public void setDataHora(String timestamp) {
		this.dataHora = timestamp;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String message) {
		this.mensagem = message;
	}

	public String getRecurso() {
		return recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

}