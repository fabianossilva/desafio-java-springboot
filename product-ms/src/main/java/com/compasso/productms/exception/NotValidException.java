package com.compasso.productms.exception;

public class NotValidException extends RuntimeException {

	private static final long serialVersionUID = 3200073089125243430L;

	public NotValidException() {
		super("Erro de validação");
	}

	public NotValidException(String msg) {
		super(msg);
	}

}
