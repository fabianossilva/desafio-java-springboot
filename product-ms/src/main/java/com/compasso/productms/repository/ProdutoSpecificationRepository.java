package com.compasso.productms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.compasso.productms.entity.Produto;

public interface ProdutoSpecificationRepository extends JpaRepository<Produto, Integer>, JpaSpecificationExecutor<Produto>  {

}
