package com.compasso.productms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "tbProduto")
public class Produto implements Serializable {

	private static final long serialVersionUID = -7840745689677307152L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
	
    @Column(name = "name",nullable = false, length = 150)
    @NotEmpty(message = "Field name is required.")
    private String name;
    
    @Column(name = "description",nullable = false)
    @NotEmpty(message = "Field description is required.")
    private String description;
    
    @Column(name = "price",nullable = false, precision = 20, scale = 2)
    @NotNull(message = "Field price is required.")
    @Positive(message = "Field price must be positive.")
    private BigDecimal price;

	
}
