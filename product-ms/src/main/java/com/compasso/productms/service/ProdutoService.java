package com.compasso.productms.service;


import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.compasso.productms.entity.Produto;
import com.compasso.productms.exception.NotFoundItemException;
import com.compasso.productms.repository.ProdutoRepository;
import com.compasso.productms.repository.ProdutoSpecificationRepository;
import com.compasso.productms.specification.ProdutoMaxPriceSpecification;
import com.compasso.productms.specification.ProdutoMinPriceSpecification;
import com.compasso.productms.specification.ProdutoNameDescriptionSpecification;

@Service
public class ProdutoService {

	@Autowired
	ProdutoRepository produtoRepository;
	
	@Autowired
	ProdutoSpecificationRepository produtoSpecificationRepository;
	
	//Método para criação de novo produto na base de dados
	public ResponseEntity<?> createProduct(HttpServletRequest request, Produto produto) {

		Produto novoProduto = this.produtoRepository.save(produto);

		return new ResponseEntity<>(novoProduto, HttpStatus.CREATED);
	}

	
	//Método para atualização de um produto na base de dados, se o mesmo existir.
	public ResponseEntity<?> updateProduct(HttpServletRequest request, @Valid Produto produto, Integer id) {

		if(!this.produtoRepository.existsById(id)) {
			throw new NotFoundItemException("Produto não encontrado");
		}
		
		produto.setId(id);
		this.produtoRepository.save(produto);
		
		return new ResponseEntity<>(produto, HttpStatus.OK);
	}

	//Método para busca de um produto na base de dados por id.
	public ResponseEntity<?> getProductById(HttpServletRequest request, Integer id) {
		
		Produto produto = this.produtoRepository.findById(id).orElseThrow(() -> new NotFoundItemException());
		
		return new ResponseEntity<>(produto, HttpStatus.OK);
	}

	//Método para busca de todos os produtos salvos no banco.
	public ResponseEntity<?> getAllProducts(HttpServletRequest request) {
		List<Produto> lista = this.produtoRepository.findAll();
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}

	//Método para apagar produto pelo id, se o mesmo existir.
	public ResponseEntity<?> deleteProduct(HttpServletRequest request, Integer id) {
		Produto produto = this.produtoRepository.findById(id).orElseThrow(() -> new NotFoundItemException());
		this.produtoRepository.delete(produto);
		return new ResponseEntity<>(HttpStatus.OK);
	}


	//Método para realizar uma busca personalizada
	public ResponseEntity<?> search(String q, BigDecimal min_price, BigDecimal max_price) {
		
		Specification<Produto> specification = Specification
                .where(ProdutoNameDescriptionSpecification.name(q))
                .and(ProdutoMinPriceSpecification.minPrice(min_price))
                .and(ProdutoMaxPriceSpecification.maxPrice(max_price));
		
		List<Produto> lista = produtoSpecificationRepository.findAll(specification);
		
		return new ResponseEntity<>(lista,HttpStatus.OK);
	}
	
	
	
	

}
