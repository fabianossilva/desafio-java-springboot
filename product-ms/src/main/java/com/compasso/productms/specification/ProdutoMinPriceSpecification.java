package com.compasso.productms.specification;

import java.math.BigDecimal;

import org.springframework.data.jpa.domain.Specification;

import com.compasso.productms.entity.Produto;

public class ProdutoMinPriceSpecification {
	
	public static Specification<Produto> minPrice(BigDecimal minPrice) {
		if (minPrice == null){
			return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        } else {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.<String>get("price"), minPrice.toString());
    }
 
	}

}
