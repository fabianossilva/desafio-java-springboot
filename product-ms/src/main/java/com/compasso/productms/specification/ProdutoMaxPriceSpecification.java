package com.compasso.productms.specification;

import java.math.BigDecimal;

import org.springframework.data.jpa.domain.Specification;

import com.compasso.productms.entity.Produto;

public class ProdutoMaxPriceSpecification {
	
	public static Specification<Produto> maxPrice(BigDecimal maxPrice) {
		if (maxPrice == null){
			return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        } else {
        return (root, criteriaQuery, criteriaBuilder) ->
        		criteriaBuilder.lessThanOrEqualTo(root.<String>get("price"), maxPrice.toString());
    }
 
	}

}
