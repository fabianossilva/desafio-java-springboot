package com.compasso.productms.specification;

import org.springframework.data.jpa.domain.Specification;

import com.compasso.productms.entity.Produto;

public class ProdutoNameDescriptionSpecification {
	
	public static Specification<Produto> name(String name) {
		if (name == null){
			return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isTrue(criteriaBuilder.literal(true));
        } else {
        return (root, criteriaQuery, criteriaBuilder) ->
        criteriaBuilder.or(
                criteriaBuilder.like(root.<String>get("name"), "%" + name + "%"),
                criteriaBuilder.like(root.<String>get("description"), "%" + name + "%")
        );
        }
    }
 
    

}
